# springboot-log4j-xml

Spring boot project with Log4j XML configuration file

## log4j.xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE log4j:configuration SYSTEM "https://logging.apache.org/log4j/1.2/apidocs/org/apache/log4j/xml/doc-files/log4j.dtd">

<log4j:configuration debug="true"
	xmlns:log4j="http://jakarta.apache.org/log4j/">

	<appender name="file" class="org.apache.log4j.DailyRollingFileAppender"> 
		<param name="append" value="true" /> 
		<param name="DatePattern" value="'.'yyyy-MM-dd"/> 
		<param name="file" value="/apps/logs/myApp.log" /> 
		<layout class="org.apache.log4j.PatternLayout"> 
			<param name="ConversionPattern" value="%d{yyyy-MM-dd HH:mm:ss} %-5p %c{1}:%L - %m%n" /> 
		</layout> 
	</appender>
   <!--
	<appender name="file"
		class="org.apache.log4j.RollingFileAppender">
		<param name="append" value="true" />
		<param name="maxFileSize" value="10KB" />
		<param name="maxBackupIndex" value="5" />
		<param name="file" value="/apps/logs/myApp.log" />
		<layout class="org.apache.log4j.PatternLayout">
			<param name="ConversionPattern"
				value="%d{yyyy-MM-dd HH:mm:ss} %-5p %C{1}:%L - %m%n" />
		</layout>

	</appender>
	-->
	
	<root>
		<level value="DEBUG" />
		<appender-ref ref="file" />
	</root>



</log4j:configuration>


