package com.okayjava.log4j.xml.controller;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

	private static final Logger logger = Logger.getLogger(IndexController.class);
	
	// Instance initializer block
	{DOMConfigurator.configure("src/main/resources/log4j.xml");}
	
	@GetMapping("/")
	public String indexPage() {
		logger.debug("Loading index page ...");
		logger.debug("debug level message");
		logger.info("info level message");
		logger.warn("warn level message");
		logger.error("ERROR level message");
		logger.fatal("Fatal level message");
		logger.trace("Trace level message");
		
		return "index";
	}
}
