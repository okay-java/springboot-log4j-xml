package com.okayjava.log4j.xml;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootLog4jXmlApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootLog4jXmlApplication.class, args);
	}

}
